String.prototype.insert = function (index, string) {
  if (index > 0) {
    return this.substring(0, index) + string + this.substr(index);
  }

  return string + this;
};

async function getBalance() {
  const owner = document.getElementById("balance-owner").value;
  pushToLog("Fetching balance...");
  const balance = await window.contract.hakToken.methods
    .balanceOf(owner)
    .call();
  pushToLog(`Balance: ${balance}`);
  var strbalance = balance + "";
  while (strbalance.length < 19) {
    strbalance = `0${strbalance}`;
  }
  document.getElementById("balance-output").value = strbalance;
}

async function refreshBalancePanel() {
  const balance = await window.contract.hakToken.methods
    .balanceOf("0x1669383c35317D6936749bf2E4a04CbccF558213")
    .call();
  var strbalance = balance + "";
  while (strbalance.length < 21) {
    strbalance = `0${strbalance}`;
  }
  strbalance = strbalance.insert(3, ".");
  document.getElementById("balance-attack-account").innerHTML = strbalance;
}

async function getAllowance() {
  pushToLog("Fetching allowance...");
  const owner = document.getElementById("allowance-owner").value;
  const spender = document.getElementById("allowance-spender").value;
  const balance = await window.contract.hakToken.methods
    .allowance(owner, spender)
    .call();
  document.getElementById("allowance-output").value = balance;
  pushToLog(`Allowance: ${balance}`);
}

var lastAccount;
async function getCurrentAccount() {
  if (lastAccount != null) return lastAccount;
  const accounts = await window.web3.eth.getAccounts();
  return (lastAccount = accounts[0]);
}

async function increaseAllowance() {
  const value = document.getElementById("increaseAllowance-amount").value;
  const spender = document.getElementById("increaseAllowance-spender").value;
  pushToLog(`Executing increaseAllowance with ${value}`);
  const account = lastAccount == null ? await getCurrentAccount() : lastAccount;
  await window.contract.hakToken.methods
    .increaseAllowance(spender, value)
    .send({ from: account });
  pushToLog("Set allowance!");
}

async function decreaseAllowance() {
  const value = document.getElementById("decreaseAllowance-amount").value;
  const spender = document.getElementById("decreaseAllowance-spender").value;
  pushToLog(`Executing decreaseAllowance with ${value}`);
  const account = await getCurrentAccount();
  await window.contract.hakToken.methods
    .decreaseAllowance(spender, value)
    .send({ from: account });
  pushToLog("Set allowance!");
}
