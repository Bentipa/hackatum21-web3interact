async function loadWeb3() {
  if (window.ethereum) {
    window.web3 = new Web3(window.ethereum);
    window.web3.eth.defaultChain = "goerli";
    window.ethereum.enable();
    pushToLog("Web3 Connection Setup!");
  }
}

async function load() {
  await loadWeb3();
  window.contract = {};
  window.contract.hakToken = await loadContract();
  window.contract.bank = {};
  window.contract.bank.blockbusters = await loadBankContract(
    "0x046D90F1614C3732Ce04D866bc9Ef0ae1Cdda509"
  );
  window.contract.bank.eggplant = await loadBankContract(
    "0xe43bd2139feaadcf196af14d289122b8a23b16a2"
  );
  //   window.contract.bank.hack = await loadBankContract(
  //     "0xAF203Ae3Da969ff19b520C8B191747055e2Cab0E"
  //   );

  pushToLog("Ready!");

  setInterval(async () => {
    await refreshBalancePanel();
  }, 5000);
}

function pushToLog(message) {
  const log = document.getElementById("log");
  log.innerHTML = `${message}\n${log.innerHTML}`;
  console.log(message);
}

load();
