async function getBankBalance() {
  const bank = document.getElementById("bank").value;
  const token = document.getElementById("bank-balance-token").value;
  pushToLog(`[${bank}] Fetching balance...`);
  if (window.contract.bank[bank] == null) {
    pushToLog("Invalid bankname: " + bank);
    return;
  }
  console.log(window.contract.bank[bank]);
  const balance = await window.contract.bank[bank].methods
    .getBalance(token)
    .call();
  pushToLog(`[${bank}] Bank-Balance: ${balance}`);
  var strbalance = balance + "";
  console.log("strbalance " + strbalance);
  while (strbalance.length < 19) {
    strbalance = `0${strbalance}`;
  }
  console.log("strbalance " + strbalance);
  document.getElementById("bank-balance-output").value = strbalance;
}

async function deposit() {
  const bank = document.getElementById("bank").value;
  const token = document.getElementById("bank-deposit-token").value;
  const amount = document.getElementById("bank-deposit-amount").value;
  pushToLog(`[${bank}] Depositing ${amount} to ${bank}`);
  if (window.contract.bank[bank] == null) {
    pushToLog("Invalid bankname: " + bank);
    return;
  }
  console.log(window.contract.bank[bank]);
  const account = await getCurrentAccount();
  pushToLog(`[${bank}] Executing increaseAllowance with ${amount}`);
  await window.contract.hakToken.methods
    .increaseAllowance(window.contract.bank[bank].options.address, amount)
    .send({ from: account });
  pushToLog(`[${bank}] Set allowance!`);
  const success = await window.contract.bank[bank].methods
    .deposit(token, amount)
    .send({ from: account });
  pushToLog(`[${bank}] Transaction successfull: ${success.status}`);
  document.getElementById("bank-deposit-output").value = success.status;
}

async function withdraw() {
  const bank = document.getElementById("bank").value;
  const token = document.getElementById("bank-withdraw-token").value;
  const amount = document.getElementById("bank-withdraw-amount").value;
  pushToLog(`[${bank}] Withdrawing ${amount} from ${bank}`);
  if (window.contract.bank[bank] == null) {
    pushToLog("Invalid bankname: " + bank);
    return;
  }
  console.log(window.contract.bank[bank]);
  const account = await getCurrentAccount();
  const success = await window.contract.bank[bank].methods
    .withdraw(token, amount)
    .send({ from: account });
  pushToLog(`[${bank}] Transaction successfull: ${success.status}`);
  document.getElementById("bank-withdraw-output").value = success.status;
}
