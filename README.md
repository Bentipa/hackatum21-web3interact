# hackatum21-web3interact

This is our web-ui to interact with some bank contracts on the goerli test network.

Disclaimer: This was rapid-prototyped during the night between 0 to 2 am so the code might be choatic, etc. but it works.
You can get some ideas on how to implement web3 interaction :)

## Setup
Just clone, run `npm install` and run a webserver. Best is to use MetaMask which should ask you to connect once you visit the page.
Then you can play around with it.
